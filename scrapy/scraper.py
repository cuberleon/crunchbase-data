import scrapy
import pandas as pd
from datetime import datetime
from pathlib import Path
import html

df = pd.read_csv('../investments_VC.csv', encoding = 'latin1')
pref = 'https://www.crunchbase.com'
urls = [pref + str(perm) for perm in df['permalink'].to_list()][:10]

res_df = pd.DataFrame(columns=['permalink', 'num_employees'])
cnt = 0
fails = 0

Path('backups').mkdir(exist_ok=True)
Path('html_bodies').mkdir(exist_ok=True)

class BrickSetSpider(scrapy.Spider):
    name = "brickset_spider"
    start_urls = urls

    custom_settings = {
        'CONCURRENT_REQUESTS': 1,
        'DOWNLOAD_DELAY': 3,
    }

    def start_requests(self):
        headers= {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:48.0) Gecko/20130101 Firefox/48.0'}
        for url in self.start_urls:
            yield scrapy.Request(url, headers=headers)

    def parse(self, response):
        if response.status != 200:
            global fails
            fails += 1
            return

        permalink = response.request.url[len(pref):]
        num_employees = response.xpath('//a[contains(@href, "/search/people/field/organizations/num_employees_enum")]/text()').get()
        html_body = response.xpath('//body/chrome').get()
        html_body = html.unescape(html_body)
        # html_body = html_body.replace('\n', '')
 
        parsed_dict = {
            'permalink': permalink,
            'num_employees': num_employees,
        }

        global res_df
        global cnt
        res_df = res_df.append(parsed_dict, ignore_index=True)
        cnt += 1

        now = datetime.now()
        dt_string = now.strftime("%Y_%m_%d_%H_%M_%S")
        
        if cnt % 2 == 0:
            res_df.to_csv(f'backups/res_{dt_string}.csv', index=False)

        with open(f"html_bodies/{permalink[1:].replace('/', '_')}.txt", 'w', encoding="utf-8") as f:
            f.write(html_body)

        yield parsed_dict

    def closed(self, reason):
        print(f'DONE\nOK={cnt}, FAILS={fails}')

        now = datetime.now()
        dt_string = now.strftime("%Y_%m_%d_%H_%M_%S")

        global res_df
        res_df.to_csv(f'res_{dt_string}.csv', index=False)
        # res_df.to_excel('res.xlsx', index=False)